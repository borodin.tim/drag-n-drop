// class ProjectInput {
//     templateElement: HTMLTemplateElement;
//     hostElement: HTMLDivElement;

//     constructor() {
//         this.templateElement = document.getElementById('project-input')! as HTMLTemplateElement;
//         this.hostElement = document.getElementById('app')! as HTMLDivElement;
//     }
// }

// interface ValidatorConfig{
//     [property: string]: {
//         [validatableProperty: string]: string[] // ['required', 'positive']
//     }
// }
// const registeredValidators: ValidatorConfig = {};

// function PositiveNumber(target: any, propertyName: string) {
//     registeredValidators[target.constructor.name] = {
//         ...registeredValidators[target.constructor.name],
//         [propertyName]: ['positive']
//     }
// }

// function Required(target: any, propertyName: string) {
//     registeredValidators[target.constructor.name] = {
//         ...registeredValidators[target.constructor.name],
//         [propertyName]: ['required']
//     }
// }

// function minFiveCharsLen(target: any, propertyName: string) {
//     registeredValidators[target.constructor.name] = {
//         ...registeredValidators[target.constructor.name],
//         [propertyName]: ['minfivecharslen']
//     }
// }

// class Project{
//     @minFiveCharsLen
//     private title: string;
    
//     @Required
//     private description: string;

//     @PositiveNumber
//     private people: number;

//     constructor(t: string, d: string, p: number) {
//         this.title = t;
//         this.description = d;
//         this.people = p;
//     }
    
//     get name() {
//         return this.title
//     }
// }

// function validate(obj: any) {
//     const objValidatorConfig = registeredValidators[obj.constructor.name];
//     if (!objValidatorConfig) {
//         return true
//     }
//     let isValid = true;
//     for (const prop in objValidatorConfig) {
//         for (const validator of objValidatorConfig[prop]) {
//             switch (validator) {
//                 case 'required':
//                     isValid = isValid && !!obj[prop];
//                     break;
//                 case 'positive':
//                     isValid = isValid && obj[prop] > 0;
//                     break;
//                 case 'minfivecharslen':
//                     isValid = isValid && obj[prop].length >= 5;
//                     break;
//             }
//         }
//     }
//     return isValid;
// }

// const projectsList: Project[] = [];

// // app
// const appEl = document.querySelector('#app')! as HTMLDivElement;

// // Form
// const formTemp = document.getElementById('project-input') as HTMLTemplateElement;
// const formEl = formTemp.content.querySelector('form')!;
// const formClone = formEl.cloneNode(true);

// formClone.addEventListener('submit', (e) => {
//     e.preventDefault();

//     const titleEl = document.querySelector('#title')! as HTMLInputElement;
//     const descriptionEl = document.querySelector('#description')! as HTMLInputElement;
//     const peopleEl = document.querySelector('#people')! as HTMLInputElement;
//     const title = titleEl.value;
//     const description = descriptionEl.value;
//     const people = +peopleEl.value;

//     const newProject = new Project(title, description, people);
//     if (!validate(newProject)) {
//         alert('Invalid input');
//         return;
//     }
//     projectsList.push(newProject);
//     console.log('🚀 ~ file: app.ts ~ line 28 ~ formClone.addEventListener ~ projectsList', projectsList);

//     const singleProjClone = singleProjTemplate.content.cloneNode(true);
//     const liEl = (singleProjClone as HTMLElement).querySelector('li')!;
//     liEl.textContent = newProject.name;
//     ulEl.appendChild(singleProjClone);
    
//     clearInputs(titleEl, descriptionEl, peopleEl);
// })
// appEl.appendChild(formClone);


// // Project list
// const projListTemplate = document.getElementById('project-list') as HTMLTemplateElement;
// const projListClone = projListTemplate.content.cloneNode(true);

// appEl.appendChild(projListClone);

// const h2El = document.body.querySelector('h2')!;
// h2El.textContent = 'Project List';

// const ulEl = document.body.querySelector('ul')!;


// // Single project
// const singleProjTemplate = document.getElementById('single-project') as HTMLTemplateElement;





// // Utility functions:

// const clearInputs = (...params: HTMLInputElement[]) => {
//     for (const p of params) {
//         p.value = '';
//     }
// }
